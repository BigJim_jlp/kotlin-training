fun main(){
    sayHello()
}

fun sayHello() {
    println("Please enter your name:")
    val name = readLine()
    println("Welcome to Kotlin, $name!")
}
