fun main() {
    multiPerson()
}

class Person(
    val firstName: String,
    val middleName: String?,
    val lastName: String,
    val height: Double,
    val hairColour: String,
    val wearsGlasses: Boolean
) {
    fun formatPerson(): String {
        var middleReturn = "";
        if (middleName == null) {
            middleReturn = " "
        } else {
            middleReturn = " $middleName "
        }
        return "$firstName$middleReturn$lastName"
    }
}

fun multiPerson() {
    val james = Person("James", "Alexander", "Holton", 200.0, "Brown", false)
    val lena = Person("Lena", "Tracey", "Dack", 150.0, "Blonde", true)
    val lottie = Person("Lottie", null, "Holton", 100.0, "Blonde", false)
    val francesca = Person("Francesca", null, "Holton", 90.0, "Red", true)
    val family = listOf(james, lena, lottie, francesca)

    for (person in family) {
        printGlassesPeople(person)
    }
}

private fun printGlassesPeople(person: Person) {
    if (person.wearsGlasses) {
        println(person.formatPerson())
    }
}

