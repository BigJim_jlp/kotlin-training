import java.io.File

fun main() {
    val rectanglesCSV = readCSVFile()
    val rectangleList = createRectangles(rectanglesCSV)

}

private fun readCSVFile(): List<String> {
    return File("rectangles.csv").readLines()
}

private fun createRectangles(rectangleCSV: List<String>) {
    for (rectangleRow in rectangleCSV) {
        val rectangleBits = rectangleRow.split(",")
        val rectangle = Rectangle(
            rectangleBits[0], rectangleBits[1].toDouble(), rectangleBits[2].toDouble()
        )
        rectangle.printDetails();
    }
}

data class Rectangle(
    var name: String,
    var width: Double,
    var height: Double
) {
    fun printDetails() {
        println(name + squareCheck() + "with area " + width * height + " and perimeter " + calculatePerimeter())
    }

    fun calculatePerimeter(): Double {
        return (width * 2) + (height * 2)
    }

    fun squareCheck(): String {
        if (width == height) {
            return " is a Square "
        } else {
            return " is a Rectangle "
        }
    }
}