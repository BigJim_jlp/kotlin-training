data class Item(
    val description: String,
    val price: Double,
    val discount: Double?
) {
    fun calculateItemTotal(): Double {
        if (discount == null) {
            return price
        } else {
            return price * discount
        }
    }
}

val shoppingBasket = listOf(
    Item("Cheese", 2.49, null),
    Item("Milk", 0.89, null),
    Item("Bread", 1.99, 0.50),
    Item("Banana", 0.25, null)
)

fun main() {
    var basketTotal = 0.0
    for (item in shoppingBasket) {
        basketTotal += item.calculateItemTotal()
    }
    println("The Basket Total = £$basketTotal")


}
