import java.io.File

fun main() {
    val savedNotes = File("notes.txt").readLines()
    processNotes(savedNotes)
}

private fun processNotes(savedNotes: List<String>) {
    for (note in savedNotes) {
        val noteBits = note.split("|")
        NotepadEntry(noteBits[0].toInt(), noteBits[1], noteBits[2], noteBits[3]).printNote()
    }
}

data class NotepadEntry(
    var dayOfMonth: Int,
    var monthName: String,
    var note: String,
    var enteredBy: String
){
    fun printNote(){
        println("Note [$note] entered by $enteredBy on $dayOfMonth $monthName")
    }
}