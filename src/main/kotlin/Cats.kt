val cats = listOf(
    Cat("Ginger", 100),
    Cat("Fred", 203),
    Cat("Luna", 43),
    Cat("Tabby", null),
    Cat("George", null),
    Cat("Greta", 84697),
    Cat("Miss Sparkles", 102),
    Cat("Galileo", 55)
)

data class Cat(
    val name: String,
    val microchipId: Int?
)

fun main() {
    println(cats.filter {it.name.substring(0,1) == "G"}.filter {it.microchipId!=null}.sortedBy { it.microchipId }.map{printChipID(it)})
}

fun printChipID(cat:Cat){
    println(cat.microchipId)
}