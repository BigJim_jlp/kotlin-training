import java.io.File

fun main() {
    val numberList = File("numbers.txt").readLines()
    println(sumNumbers(numberList))
}

private fun sumNumbers(numberList: List<String>): Int {
    var numberTotal = 0
    for (number in numberList) {
        numberTotal += number.toInt()
    }
    return numberTotal
}