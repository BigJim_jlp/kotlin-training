import java.io.File

fun main() {
    val yearData = File("Death_rates.csv").readLines().filter { it.contains("All Races,Both Sexes") }
    var listOfDeathRates = processYears(yearData)
    println(listOfDeathRates)
}

private fun processYears(yearData: List<String>): List<YearDeathRates> {
    var listOfDeathData = mutableListOf<YearDeathRates>()
    for (year in yearData) {
        val dataBits = year.split(",")
        listOfDeathData.add(YearDeathRates(dataBits[0], dataBits[1], dataBits[2], dataBits[3], dataBits[4]))
    }
    return listOfDeathData
}

data class YearDeathRates(
    var year: String,
    var race: String,
    var sex: String,
    var averageLifeExpectancy: String,
    var deathRate: String
)